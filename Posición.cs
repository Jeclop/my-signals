﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;

namespace MySignals
{
    
    public partial class Posición : Form
    {
        
        public string PORT;
        private delegate void DelegadoAcceso(string accion);
        string burffeint;
        List<string> posicion = new List<string>();
        StringBuilder cvscon = new StringBuilder();
        string carpeta, pos="",ace="";
        bool re_posicion = false,re_acele=false,guarda = false;
        public Posición()
        {
            InitializeComponent();
        }


        private void AccesoForm(string accion)
        {
            burffeint = accion;
            caja1.Text = burffeint;

        }

        private void interrepcion(string accion)
        {

            DelegadoAcceso var_dele;
            var_dele = new DelegadoAcceso(AccesoForm);
            object[] arg = { accion };
            base.Invoke(var_dele, arg);
        }
        private void boton_ok_Click(object sender, EventArgs e)
        {
            
           serialPort1.PortName = PORT;
           if (boton_ok.Text == "OK")
           {
               serialPort1.Open();

               serialPort1.Write("#");
               boton_ok.Text = "Conectado...";
               boton_ok.Enabled = false;
               terminar.Enabled = true;

           }
           else { }
         
        
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            
            if (Convert.ToChar(serialPort1.ReadChar()) == '#'){re_posicion=true;}
           if(re_posicion)
            {
                 pos = serialPort1.ReadLine();
                 pos = pos.Replace("\r", string.Empty).Replace("\n", string.Empty);
                 re_posicion = false;
                 re_acele = true;

            }
           else if (re_acele)
           {
               ace = serialPort1.ReadLine();
               ace = ace.Replace("\r", string.Empty).Replace("\n", string.Empty);
               re_acele = false;
               guarda = true;
           
           }
           if (guarda)
            {   
                cvscon.AppendLine(pos+","+ ace);
                interrepcion("Posicion del cuerpo: "+pos);
                guarda = false;
            }
         
        }

        private void Posición_Load(object sender, EventArgs e)
        {
            terminar.Enabled = false;
            cvscon.AppendLine("Posicion,X,Y,Z");
          
        }

        private void terminar_Click(object sender, EventArgs e)
        {
            guardar();

        }

        private void Posición_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (boton_ok.Text == "OK")
            {
                e.Cancel = false;

            }
            else
            {
                DialogResult result = MessageBox.Show("¿Seguro que dese salir?", "Salir", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    DialogResult res = MessageBox.Show("¿Desea guardar los datos recopilados?", "Guardar", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        guardar();
                        e.Cancel = false;
                    }
                    if (res == DialogResult.No)
                    {

                        e.Cancel = false;
                    }
                }
                else if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }

            }
        }

        private void guardar()
        {

            if (boton_ok.Text == "Conectado...")
            {

                SaveFileDialog salvar = new SaveFileDialog();
                salvar.Filter = "Archivos csv(*.csv)|*.csv";
                salvar.Title = "Guardar";

                DialogResult result = salvar.ShowDialog();

                if (result == DialogResult.OK)
                {
                    serialPort1.Write("$");

                    serialPort1.Close();
                    carpeta = salvar.FileName;
                    salvar.Dispose();

                    carpeta = carpeta.Replace("\\", "\\\\");
                    File.AppendAllText(carpeta, cvscon.ToString());
                    boton_ok.Text = "OK";
                    terminar.Enabled = false;
                    boton_ok.Enabled = true;
                    cvscon.Clear();

                }
                else if (result == DialogResult.Cancel) { return; }


            }

        }
  

     
    }
}
