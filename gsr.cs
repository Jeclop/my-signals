﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySignals
{
    public partial class gsr : Form
    {
        public string PORT, carpeta, burffeint, d;
        private delegate void DelegadoAcceso(string accion);
        StringBuilder cvscon = new StringBuilder();
        bool re_conductancia = false, re_resistencia = false, re_conduc_voltaje = false, guarda = false;
        string conduc, resis, volta;
        public gsr()
        {
            InitializeComponent();
        }

        private void boton_ok_Click(object sender, EventArgs e)
        {
            serialPort1.PortName = PORT;
            if (boton_ok.Text == "OK")
            {
                serialPort1.Open();

                serialPort1.Write("#");
                boton_ok.Text = "Conectado...";
                boton_ok.Enabled = false;
                terminar.Enabled = true;
                re_conductancia = true;
            }
            else { }
        }

        private void AccesoForm(string accion)
        {
            burffeint = accion;
            caja1.Text = burffeint;
            cvscon.Append(d);


        }

        private void interrepcion(string accion)
        {

            DelegadoAcceso var_dele;
            var_dele = new DelegadoAcceso(AccesoForm);
            object[] arg = { accion };
            base.Invoke(var_dele, arg);
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (re_conductancia) {
                conduc = serialPort1.ReadLine();
                conduc = conduc.Replace("\r", string.Empty).Replace("\n", string.Empty);
                re_conductancia = false;
                re_resistencia = true;
            }
            else if(re_resistencia){
                resis = serialPort1.ReadLine();
                resis = resis.Replace("\r", string.Empty).Replace("\n", string.Empty);
                re_resistencia = false;
                re_conduc_voltaje = true;
            
            }
            else if (re_conduc_voltaje) {
                volta = serialPort1.ReadLine();
                volta = volta.Replace("\r", string.Empty).Replace("\n", string.Empty);
                re_conduc_voltaje = false;
                guarda = true;
            
            }
            if (guarda) {
                
                cvscon.AppendLine(conduc + "," + resis + "," + volta);
                interrepcion("GSR :"+conduc+", "+resis+", "+volta);
                guarda = false;
                re_conductancia = true;
                        
            }
        }

        private void terminar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void gsr_Load(object sender, EventArgs e)
        {
            terminar.Enabled = false;
            cvscon.AppendLine("Conductancia, resistencia,Voltaje de conductancia");
        }

        private void gsr_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (boton_ok.Text == "OK")
            {
                e.Cancel = false;

            }
            else
            {
                DialogResult result = MessageBox.Show("¿Seguro que dese salir?", "Salir", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    DialogResult res = MessageBox.Show("¿Desea guardar los datos recopilados?", "Guardar", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        guardar();
                        e.Cancel = false;
                    }
                    if (res == DialogResult.No)
                    {

                        e.Cancel = false;
                    }
                }
                else if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }

            }
        }

        private void guardar()
        {

            if (boton_ok.Text == "Conectado...")
            {

                SaveFileDialog salvar = new SaveFileDialog();
                salvar.Filter = "Archivos csv(*.csv)|*.csv";
                salvar.Title = "Guardar";

                DialogResult result = salvar.ShowDialog();

                if (result == DialogResult.OK)
                {
                    serialPort1.Write("$");

                    serialPort1.Close();
                    carpeta = salvar.FileName;
                    salvar.Dispose();

                    carpeta = carpeta.Replace("\\", "\\\\");
                    File.AppendAllText(carpeta, cvscon.ToString());
                    boton_ok.Text = "OK";
                    terminar.Enabled = false;
                    boton_ok.Enabled = true;
                    cvscon.Clear();

                }
                else if (result == DialogResult.Cancel) { return; }


            }

        }


    }
}
