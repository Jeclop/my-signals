﻿namespace MySignals
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conexiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.puertos = new System.Windows.Forms.ToolStripMenuItem();
            this.ComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.sensoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ECGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EMGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flujoDeAireToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.posicionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pulsioxiometroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.respuestaGalvanicaDeLaPielToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ronquidoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.temperturaCorporalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.conexiónToolStripMenuItem,
            this.sensoresToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(369, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salirToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "Archivo";
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(96, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // conexiónToolStripMenuItem
            // 
            this.conexiónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.puertos});
            this.conexiónToolStripMenuItem.Name = "conexiónToolStripMenuItem";
            this.conexiónToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.conexiónToolStripMenuItem.Text = "Conexión";
            // 
            // puertos
            // 
            this.puertos.BackColor = System.Drawing.SystemColors.Control;
            this.puertos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ComboBox1});
            this.puertos.Name = "puertos";
            this.puertos.Size = new System.Drawing.Size(117, 22);
            this.puertos.Text = "Arduino";
            this.puertos.Click += new System.EventHandler(this.puertoDeArduinoToolStripMenuItem_Click);
            // 
            // ComboBox1
            // 
            this.ComboBox1.BackColor = System.Drawing.SystemColors.Window;
            this.ComboBox1.Name = "ComboBox1";
            this.ComboBox1.Size = new System.Drawing.Size(121, 23);
            this.ComboBox1.Click += new System.EventHandler(this.ComboBox1_Click);
            // 
            // sensoresToolStripMenuItem
            // 
            this.sensoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ECGToolStripMenuItem,
            this.EMGToolStripMenuItem,
            this.flujoDeAireToolStripMenuItem,
            this.posicionToolStripMenuItem,
            this.pulsioxiometroToolStripMenuItem,
            this.respuestaGalvanicaDeLaPielToolStripMenuItem,
            this.ronquidoToolStripMenuItem1,
            this.temperturaCorporalToolStripMenuItem});
            this.sensoresToolStripMenuItem.Name = "sensoresToolStripMenuItem";
            this.sensoresToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.sensoresToolStripMenuItem.Text = "Sensores";
            // 
            // ECGToolStripMenuItem
            // 
            this.ECGToolStripMenuItem.Name = "ECGToolStripMenuItem";
            this.ECGToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.ECGToolStripMenuItem.Text = "Electrocardiografia(ECG)";
            this.ECGToolStripMenuItem.Click += new System.EventHandler(this.ECGToolStripMenuItem_Click);
            // 
            // EMGToolStripMenuItem
            // 
            this.EMGToolStripMenuItem.Name = "EMGToolStripMenuItem";
            this.EMGToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.EMGToolStripMenuItem.Text = "Electromiografia(EMG)";
            this.EMGToolStripMenuItem.Click += new System.EventHandler(this.EMGToolStripMenuItem_Click);
            // 
            // flujoDeAireToolStripMenuItem
            // 
            this.flujoDeAireToolStripMenuItem.Name = "flujoDeAireToolStripMenuItem";
            this.flujoDeAireToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.flujoDeAireToolStripMenuItem.Text = "Flujo de aire";
            this.flujoDeAireToolStripMenuItem.Click += new System.EventHandler(this.flujoDeAireToolStripMenuItem_Click);
            // 
            // posicionToolStripMenuItem
            // 
            this.posicionToolStripMenuItem.Name = "posicionToolStripMenuItem";
            this.posicionToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.posicionToolStripMenuItem.Text = "Posición del cuerpo";
            this.posicionToolStripMenuItem.Click += new System.EventHandler(this.posicionToolStripMenuItem_Click);
            // 
            // pulsioxiometroToolStripMenuItem
            // 
            this.pulsioxiometroToolStripMenuItem.Name = "pulsioxiometroToolStripMenuItem";
            this.pulsioxiometroToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.pulsioxiometroToolStripMenuItem.Text = "SPO2/Pulsioxiometro";
            this.pulsioxiometroToolStripMenuItem.Click += new System.EventHandler(this.pulsioxiometroToolStripMenuItem_Click);
            // 
            // respuestaGalvanicaDeLaPielToolStripMenuItem
            // 
            this.respuestaGalvanicaDeLaPielToolStripMenuItem.Name = "respuestaGalvanicaDeLaPielToolStripMenuItem";
            this.respuestaGalvanicaDeLaPielToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.respuestaGalvanicaDeLaPielToolStripMenuItem.Text = "Respuesta galvanica de la piel";
            this.respuestaGalvanicaDeLaPielToolStripMenuItem.Click += new System.EventHandler(this.respuestaGalvanicaDeLaPielToolStripMenuItem_Click);
            // 
            // ronquidoToolStripMenuItem1
            // 
            this.ronquidoToolStripMenuItem1.Name = "ronquidoToolStripMenuItem1";
            this.ronquidoToolStripMenuItem1.Size = new System.Drawing.Size(230, 22);
            this.ronquidoToolStripMenuItem1.Text = "Ronquido";
            this.ronquidoToolStripMenuItem1.Click += new System.EventHandler(this.ronquidoToolStripMenuItem1_Click);
            // 
            // temperturaCorporalToolStripMenuItem
            // 
            this.temperturaCorporalToolStripMenuItem.Name = "temperturaCorporalToolStripMenuItem";
            this.temperturaCorporalToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.temperturaCorporalToolStripMenuItem.Text = "Tempertura corporal";
            this.temperturaCorporalToolStripMenuItem.Click += new System.EventHandler(this.temperturaCorporalToolStripMenuItem_Click);
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 353);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormPrincipal";
            this.Text = "Mysignals Innova";
            this.Load += new System.EventHandler(this.FormPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conexiónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sensoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem puertos;
        private System.Windows.Forms.ToolStripMenuItem ECGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EMGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flujoDeAireToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem posicionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pulsioxiometroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem respuestaGalvanicaDeLaPielToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ronquidoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem temperturaCorporalToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox ComboBox1;
    }
}

