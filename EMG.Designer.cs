﻿namespace MySignals
{
    partial class EMG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.boton_ok = new System.Windows.Forms.Button();
            this.caja1 = new System.Windows.Forms.TextBox();
            this.terminar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 115200;
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // boton_ok
            // 
            this.boton_ok.Font = new System.Drawing.Font("Siemens Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boton_ok.Location = new System.Drawing.Point(161, 57);
            this.boton_ok.Name = "boton_ok";
            this.boton_ok.Size = new System.Drawing.Size(96, 33);
            this.boton_ok.TabIndex = 1;
            this.boton_ok.Text = "OK";
            this.boton_ok.UseVisualStyleBackColor = true;
            this.boton_ok.Click += new System.EventHandler(this.boton_ok_Click);
            // 
            // caja1
            // 
            this.caja1.Location = new System.Drawing.Point(25, 113);
            this.caja1.Name = "caja1";
            this.caja1.Size = new System.Drawing.Size(232, 20);
            this.caja1.TabIndex = 2;
            // 
            // terminar
            // 
            this.terminar.Font = new System.Drawing.Font("Siemens Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.terminar.Location = new System.Drawing.Point(91, 167);
            this.terminar.Name = "terminar";
            this.terminar.Size = new System.Drawing.Size(95, 32);
            this.terminar.TabIndex = 4;
            this.terminar.Text = "Terminar";
            this.terminar.UseVisualStyleBackColor = true;
            this.terminar.Click += new System.EventHandler(this.terminar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Siemens Serif Semibold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Recopilar datos";
            // 
            // EMG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.terminar);
            this.Controls.Add(this.caja1);
            this.Controls.Add(this.boton_ok);
            this.Name = "EMG";
            this.Text = "EMG";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EMG_FormClosing);
            this.Load += new System.EventHandler(this.EMG_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button boton_ok;
        private System.Windows.Forms.TextBox caja1;
        private System.Windows.Forms.Button terminar;
        private System.Windows.Forms.Label label1;
    }
}