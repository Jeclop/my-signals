﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace MySignals
{
    public partial class FormPrincipal : Form
    {
        public string port;
        public string p = "holaaa";
             
        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void puertoDeArduinoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] com = SerialPort.GetPortNames();
            ComboBox1.Items.Clear();
            foreach (string mostrar in com)
            {
                ComboBox1.Items.Add(mostrar);                
            }

         
           
               
           
        }

     

        private void posicionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Posición abrir = new Posición();
            abrir.MdiParent = this;
            abrir.PORT = port;
            abrir.Show();
        }

        private void ComboBox1_Click(object sender, EventArgs e)
        {
            ComboBox1.SelectedIndexChanged += new System.EventHandler(ComboBox1_SelectedIndexChanged);
        }


        private void ComboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            port = ComboBox1.Text;          
            
        }

     
        private void ECGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ECG abrir = new ECG();
            abrir.MdiParent = this;
            abrir.PORT = port;
            abrir.Show();
        }

        private void EMGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EMG abrir = new EMG();
            abrir.MdiParent = this;
            abrir.PORT = port;
            abrir.Show();
        }

  
        private void flujoDeAireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            flujo_de_aire abrir = new flujo_de_aire();
            abrir.MdiParent = this;
            abrir.PORT = port;
            abrir.Show();
        }

      
        private void pulsioxiometroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pulsioximetro abrir = new pulsioximetro();
            abrir.MdiParent = this;
            abrir.PORT = port;
            abrir.Show();
        }

        private void respuestaGalvanicaDeLaPielToolStripMenuItem_Click(object sender, EventArgs e)
        {
            gsr abrir = new gsr();
            abrir.MdiParent = this;
            abrir.PORT = port;
            abrir.Show();
        }

        private void ronquidoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ronquido abrir = new ronquido();
            abrir.MdiParent = this;
            abrir.PORT = port;
            abrir.Show();
        }

        private void temperturaCorporalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            temperatura abrir = new temperatura();
            abrir.MdiParent = this;
            abrir.PORT = port;
            abrir.Show();
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            CenterWindowOnScreen();
        }

        private void CenterWindowOnScreen()
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = System.Convert.ToInt32((screenWidth / 2) - (windowWidth / 2));
            this.Top = System.Convert.ToInt32((screenHeight / 2) - (windowHeight / 2));
        }


       

        

       
        

     
    }
}
