﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySignals
{
    public partial class flujo_de_aire : Form
    {
        public string PORT, carpeta, burffeint, d;
        private delegate void DelegadoAcceso(string accion);
        StringBuilder cvscon = new StringBuilder();
        public flujo_de_aire()
        {
            InitializeComponent();
        }

        private void boton_ok_Click(object sender, EventArgs e)
        {
            serialPort1.PortName = PORT;

            if (boton_ok.Text == "OK")
            {
                serialPort1.Open();

                serialPort1.Write("#");
                boton_ok.Text = "Conectado...";
                boton_ok.Enabled = false;
                terminar.Enabled = true;

            }
            else { }
        }

        private void terminar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            try
            {
                d = serialPort1.ReadLine();

                interrepcion("Airflow value :  " + serialPort1.ReadLine());

            }
            catch (Exception ex) { }
        }

        private void AccesoForm(string accion)
        {
            burffeint = accion;
            caja1.Text = burffeint;
            cvscon.Append(d);


        }

        private void interrepcion(string accion)
        {

            DelegadoAcceso var_dele;
            var_dele = new DelegadoAcceso(AccesoForm);
            object[] arg = { accion };
            base.Invoke(var_dele, arg);
        }

        private void flujo_de_aire_Load(object sender, EventArgs e)
        {
            terminar.Enabled = false;
            cvscon.AppendLine("Airflow(V)");
        }
        
        private void guardar()
        {

            if (boton_ok.Text == "Conectado...")
            {

                SaveFileDialog salvar = new SaveFileDialog();
                salvar.Filter = "Archivos csv(*.csv)|*.csv";
                salvar.Title = "Guardar";

                DialogResult result = salvar.ShowDialog();

                if (result == DialogResult.OK)
                {
                    serialPort1.Write("$");

                    serialPort1.Close();
                    carpeta = salvar.FileName;
                    salvar.Dispose();

                    carpeta = carpeta.Replace("\\", "\\\\");
                    File.AppendAllText(carpeta, cvscon.ToString());
                    boton_ok.Text = "OK";
                    terminar.Enabled = false;
                    boton_ok.Enabled = true;
                    cvscon.Clear();

                }
                else if (result == DialogResult.Cancel) { return; }


            }

        }

        private void flujo_de_aire_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (boton_ok.Text == "OK")
            {
                e.Cancel = false;

            }
            else
            {
                DialogResult result = MessageBox.Show("¿Seguro que dese salir?", "Salir", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    DialogResult res = MessageBox.Show("¿Desea guardar los datos recopilados?", "Guardar", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        guardar();
                        e.Cancel = false;
                    }
                    if (res == DialogResult.No)
                    {

                        e.Cancel = false;
                    }
                }
                else if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }

            }
        }

    }
}
