﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MySignals
{
    public partial class pulsioximetro : Form
    {
        public string PORT;
        private delegate void DelegadoAcceso(string accion);
        string burffeint;

        StringBuilder cvscon = new StringBuilder();
        string carpeta, bpm = "", spo2 = "";
        bool datos = true, re_bpm = false, re_spo2 = false;
        public pulsioximetro()
        {
            InitializeComponent();
        }

        private void boton_ok_Click(object sender, EventArgs e)
        {

            serialPort1.PortName = PORT;

            if (boton_ok.Text == "OK")
            {
                serialPort1.Open();

                serialPort1.Write("#");
                boton_ok.Text = "Conectado...";
                boton_ok.Enabled = false;
                terminar.Enabled = true;

            }
            else { }
        }

        private void terminar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if (datos)
            {
                if (Convert.ToChar(serialPort1.ReadChar()) == '#') { re_bpm = true; datos = false; }
             
            }
            if (re_bpm)
            {
                bpm = serialPort1.ReadLine();
                bpm = bpm.Replace("\r", string.Empty).Replace("\n", string.Empty);
                re_bpm = false;
                re_spo2 = true;

            }
            else if (re_spo2)
            {
                spo2 = serialPort1.ReadLine();
                spo2 = spo2.Replace("\r", string.Empty).Replace("\n", string.Empty);
                re_spo2 = false;
               
                cvscon.AppendLine(bpm + "," + spo2);
                interrepcion("BPM: " + bpm + ",SPO2: " + spo2);
                datos = true;


            }
            
                
            
        }

        private void AccesoForm(string accion)
        {
            burffeint = accion;
            caja1.Text = burffeint;
         


        }

        private void interrepcion(string accion)
        {

            DelegadoAcceso var_dele;
            var_dele = new DelegadoAcceso(AccesoForm);
            object[] arg = { accion };
            base.Invoke(var_dele, arg);
        }

        private void pulsioximetro_Load(object sender, EventArgs e)
        {
            terminar.Enabled = false;
            cvscon.AppendLine("BPM,SPO2");
        }

        private void pulsioximetro_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (boton_ok.Text == "OK")
            {
                e.Cancel = false;

            }
            else
            {
                DialogResult result = MessageBox.Show("¿Seguro que dese salir?", "Salir", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    DialogResult res = MessageBox.Show("¿Desea guardar los datos recopilados?", "Guardar", MessageBoxButtons.YesNo);
                    if (res == DialogResult.Yes)
                    {
                        guardar();
                        e.Cancel = false;
                    }
                    if (res == DialogResult.No)
                    {

                        e.Cancel = false;
                    }
                }
                else if (result == DialogResult.No)
                {
                    e.Cancel = true;
                }

            }
        }

        private void guardar()
        {

            if (boton_ok.Text == "Conectado...")
            {

                SaveFileDialog salvar = new SaveFileDialog();
                salvar.Filter = "Archivos csv(*.csv)|*.csv";
                salvar.Title = "Guardar";

                DialogResult result = salvar.ShowDialog();

                if (result == DialogResult.OK)
                {
                    serialPort1.Write("$");

                    serialPort1.Close();
                    carpeta = salvar.FileName;
                    salvar.Dispose();

                    carpeta = carpeta.Replace("\\", "\\\\");
                    File.AppendAllText(carpeta, cvscon.ToString());
                    boton_ok.Text = "OK";
                    terminar.Enabled = false;
                    boton_ok.Enabled = true;
                    cvscon.Clear();

                }
                else if (result == DialogResult.Cancel) { return; }


            }

        }




    }
}
